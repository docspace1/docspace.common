namespace DocSpace.Domain;

public class Entity<TPrimaryKey>
{
    public TPrimaryKey ID { get; set; }
}