namespace DocSpace.Domain;

public class AuditEntity<TPrimaryKey> : Entity<TPrimaryKey>
{
    public DateTime CreatedDate { get; set; }
    public DateTime UpdatedDate { get; set; }
}