using DocSpace.Domain;

namespace DocSpace.Persistence;

public interface IRepository<TEntity, TPrimaryKey> where TEntity : Entity<TPrimaryKey>
{
    Task<IEnumerable<TEntity>> GetAll(CancellationToken cancellationToken);
    Task<TEntity> Get(TPrimaryKey id, CancellationToken cancellationToken);
    Task<TEntity> Add(TEntity entity, CancellationToken cancellationToken);
    Task<TEntity> Remove(TEntity entity, CancellationToken cancellationToken);
}